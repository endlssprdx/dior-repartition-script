#!/sbin/sh
#
# Copyright (C) 2018 Unlegacy Android Project
# Copyright (C) 2018 Svyatoslav Ryhel
# Copyright (C) 2020 Carles Romagosa
#
# Made for Dior
#

umount /system
umount /data
umount /sdcard
umount /cache

parted /dev/block/mmcblk0 <<EOF
  rm 27
  rm 28
  rm 29
  mkpart primary 335539200B 1577000447B  
  name 27 system
  mkpart primary 1577000448B 1980000255B
  name 28 cache
  mkpart primary 1980000256B 7818165759B  
  name 29 userdata
  quit
EOF

make_ext4fs /dev/block/mmcblk0p27
make_ext4fs /dev/block/mmcblk0p28
make_ext4fs /dev/block/mmcblk0p29

ln -sf /dev/block/mmcblk0p27 system
ln -sf /dev/block/mmcblk0p28 cache
ln -sf /dev/block/mmcblk0p29 userdata

rm /sbin/parted
reboot recovery
