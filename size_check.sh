#!/sbin/sh
#
# Copyright (C) 2018 Unlegacy Android Project
# Copyright (C) 2018 Svyatoslav Ryhel
# Copyright (C) 2020 Carles Romagosa
#
# Made for Dior
#

parted /dev/block/mmcblk0 unit b p quit -> /tmp/part

touch /tmp/size.prop
if grep "6207553024B" /tmp/part > /dev/null
then
  echo "data.type=original" >> /tmp/size.prop
else
  if grep "5838165504B" /tmp/part > /dev/null
  then
    echo "data.type=modified" >> /tmp/size.prop
  else
    echo "data.type=unknown" >> /tmp/size.prop
  fi
fi

rm /tmp/part
